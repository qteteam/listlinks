<?php

/*
Plugin Name: List Links
Plugin URI: https://getqte.se
Description: Download a csv of all the links
Version: 1.0
Author: Jakob Råhlén
Author URI: https://getqte.se
License: GLP2
*/

namespace ListLinks;

class ListLinks {

    /**
     * Constructor
     */
    public function __construct() {

        if (isset($_GET['links'])) {


            $csv = $this->generate_csv();
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"report.csv\";");
            header("Content-Transfer-Encoding: binary");

            echo $csv;
            exit;
        }

// Add extra menu items for admins
        add_action('admin_menu', array($this, 'admin_menu'));

// Create end-points
        add_filter('query_vars', array($this, 'query_vars'));
        add_action('parse_request', array($this, 'parse_request'));
    }

    /**
     * Add extra menu items for admins
     */
    public function admin_menu() {
        add_menu_page('Download LinkList', 'Download LinkList', 'manage_options', 'linklist', array($this, 'download_report'));
    }

    /**
     * Allow for custom query variables
     */
    public function query_vars($query_vars) {
        $query_vars[] = 'linklist';
        return $query_vars;
    }

    /**
     * Parse the request
     */
    public function parse_request(&$wp) {
        if (array_key_exists('linklist', $wp->query_vars)) {
            $this->download_report();
            exit;
        }
    }

    /**
     * Download report
     */
    public function download_report() {
        echo '<div class="wrap">';
        echo '<div id="icon-tools" class="icon32">
</div>';
        echo '<h2>Download links</h2>';
        echo '<p><a href="?page=linklist&links=all">Download link list</a></p>';
    }

    /**
     * Converting data to CSV
     */
    public function generate_csv() {
        require_once( '../wp-load.php' );
        require_once( ABSPATH . "wp-includes/pluggable.php" );
        global $post, $wp_rewrite;
        $wp_rewrite = new \wp_rewrite;
        $csv_output = '';
        $posttypes = \get_post_types(array(
            'public' => true,
        ));

        foreach($posttypes as $type) {
            $args = [
                'post_type' => $type,
                'posts_per_page' => -1,

            ];
            $posts = get_posts($args);
            foreach($posts as $qp) {
                $post = $qp;

                $csv_output .= $type . ',' . $qp->post_title . ',' . get_the_permalink($qp) . ',' . get_the_date('Y-m-d', $qp) . "\r\n";
            }

        }
        return $csv_output;
    }

}

// Instantiate a singleton of this plugin
$csvExport = new ListLinks();